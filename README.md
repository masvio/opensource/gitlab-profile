# MASV developer resources

Welcome to the public MASV repositories!

MASV transfers huge files quickly, reliably, and securely. Here are resources to help you integrate MASV with your application or workflow and get the maximum out of MASV.

Learn more about MASV at [developer.massive.io](https://developer.massive.io/).

![MASV banner](./masv-banner.png)
